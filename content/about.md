---
title: "About"
date: "2018-02-18"
---

My name is Oliver Reiter, I work (and study) somewhere in the
intersection of economics / statistics / scientific computing.

I use this blog to keep track of some topics that I'm working on in my
free time.

Currently, I am affiliated with the [wiiw](https://www.wiiw.ac.at).

You can find my CV [here](/../blog/cv/CV_OReiter_en.pdf).
